﻿namespace GameBackend;

public class Vec2
{
    public float X { get; set; }
    public float Y { get; set; }

    public Vec2(float initialX, float initialY)
    {
        X = initialX;
        Y = initialY;
    }

}


public class Ball
{
    public Vec2 Position { get; }

    public float Width { get; set; }
    public float Height { get; set; }

    private float _screenWidth { get; set; }
    private float _screenHeight { get; set; }

    private Vec2 velocityMods { get; set; }

    public Ball(float initialX, float initialY, float width, float height, float screenWidth = 0, float screenHeight = 0)
    {
        Position = new Vec2(initialX, initialY);
        Width = width;
        Height = height;
        _screenWidth = screenWidth;
        _screenHeight = screenHeight;
        velocityMods = new Vec2(1, 1);
    }

    public void Move(Vec2 velocity)
    {


        if (Position.X <= 0)
        {
            velocityMods.X = 1;

        }
        if (Position.X + Width > _screenWidth)
        {
            velocityMods.X = -1;
            
        }

        if (Position.Y <= 0)
        {
            velocityMods.Y = 1;
          
        }
        if (Position.Y + Height > _screenHeight )
        {
            velocityMods.Y = -1;
          
        }


        Position.X += velocity.X * velocityMods.X;
        Position.Y += velocity.Y * velocityMods.Y;
    }
}
