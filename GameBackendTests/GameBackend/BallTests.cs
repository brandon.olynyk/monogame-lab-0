﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameBackend.Tests
{
    [TestClass()]
    public class BallTests
    {
        [TestMethod()]
        public void BallTest()
        {
            Ball ball = new Ball(2, 4, 1, 1, 1920, 1080);
            Assert.IsNotNull(ball);
            Assert.AreEqual(ball.Position.X, 2);
            Assert.AreEqual(ball.Position.Y, 4);
        }

        [TestMethod()]
        public void MoveTest()
        {
            Ball ball = new Ball(2, 4, 1, 1, 1920, 1080);
            Assert.IsNotNull(ball);
            ball.Move(new Vec2(1, 1));
            Assert.AreEqual(ball.Position.X, 3);
        }
    }
}