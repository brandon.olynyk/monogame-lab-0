﻿using GameBackend;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace BouncingBallGame
{
    internal class BallSprite : DrawableGameComponent
    {

        //private Ball _ball;
        
        private List<Ball> _balls = new List<Ball>();

        private MouseState _previous;
        private MouseState _current;

        
        private SpriteBatch _spriteBatch;
        private Texture2D _ballImage;
        Game _game;




        public BallSprite(Game game) : base(game)
        {
            _game = game;
        }

        public override void Initialize()
        {
            _current = Mouse.GetState();
            _previous = Mouse.GetState();

            //_ball = new Ball(1, 1, 16, 16, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            //_ball.Move(new Vec2(7, 5));

            _previous = _current;
            _current = Mouse.GetState();
            if(_current.LeftButton == ButtonState.Pressed && _previous.LeftButton == ButtonState.Released)
            {
                _balls.Add(new Ball(_current.Position.X, _current.Position.Y, 16, 16, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height));
            }

            /*
             uncomment for funny ball spam on mouse hold
            
            if (_current.LeftButton == ButtonState.Pressed)
            {

                    for (int i = -10; i < 10; i++)
                    {
                        for (int j = -10; j < 10; j++)
                            _balls.Add(new Ball(_current.Position.X+(16*j), _current.Position.Y+(16*i), 16, 16, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height));
                    }
            }
             */

            foreach (Ball b in _balls)
            {
                b.Move(new Vec2(7, 4));
            }

            base.Update(gameTime);
        }

        protected override void LoadContent()
        {

            _spriteBatch = new SpriteBatch(GraphicsDevice);
            _ballImage = _game.Content.Load<Texture2D>("circle-red");

            base.LoadContent();
        }

        public override void Draw(GameTime gameTime)
        {

            _spriteBatch.Begin();
            foreach (Ball b in _balls)
            {
                _spriteBatch.Draw(_ballImage, new Vector2(b.Position.X, b.Position.Y), Color.White);
            }
            _spriteBatch.End();

            base.Draw(gameTime);
        }



    }
}
